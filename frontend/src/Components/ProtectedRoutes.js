import React, {useContext, useEffect, useState} from 'react';
import {Navigate, Outlet} from "react-router";
import {UserContext} from "../context/UserContext";

const ProtectedRoutes = (data) => {


    const {loggedIn} = useContext(UserContext)
    const {cookieUser} = useContext(UserContext)


    useEffect(() => {

    }, [])


    return (
        <div>
            {loggedIn ? <Outlet /> : <Navigate to="/login" />}
        </div>
    );
};

export default ProtectedRoutes;