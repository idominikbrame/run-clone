import React from 'react';

const Missing = () => {
    return (
        <div>
            <h1>Can't find what you're looking for</h1>
        </div>
    );
};

export default Missing;