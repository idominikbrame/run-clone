import React, {useContext, useEffect, useState, useRef} from 'react';
import styled from 'styled-components/macro';
import Background from "./adp-login-background.png"
import {FaLock} from "react-icons/fa";
import {AiFillInfoCircle, AiFillWarning, AiOutlineClose} from 'react-icons/ai'
import Logo from "./runLogoLogin.png"
import Axios from "axios";
import {useNavigate} from "react-router";
import {UserContext} from "../../context/UserContext";
import {useQuery} from "@tanstack/react-query";


const loginSelectors = {
    boxShadowColor: "#042493"
}

//Styled Components
const LoginPageContainer = styled.div `
    display: flex;
    overflow: hidden;
    position: fixed;
    margin: 0;
    padding: 0;
    width: 100%;
    height: 100vh;
    justify-content: center;
    align-items: center;
    background: url(${Background}), linear-gradient(rgba(87, 75, 75, 0.07), rgba(87, 75, 75, 0.07));
    background-repeat: no-repeat;
    background-position: center center;
    background-blend-mode: multiply;

`

const LoginPageLogo = styled.img `
    position: absolute;
    top: 30px;
    left: 55px;
    height: 33px;
    width: auto;
`

const LoginContainer = styled.div `
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    background-color: white;
    width: 470px;
    height: 600px;
    position: relative;
`

const LockIconDiv = styled.div `
    position: absolute;
    top:10px;
    left: 10px;
`
const LoginHeader = styled.header `
    font-size: 28px;
    font-weight: 400;
    margin: 30px 0 20px 0;
    
`

const InputContainer = styled.div `
    width: 300px;
    flex: 1;
    position: relative;
    box-sizing: border-box;
    margin: 0 auto;
`

const Label = styled.label `
    align-self: flex-start;
    top: 0;
    left: 0;
    align-items: flex-start;
    display: inline-block;
    padding-bottom: 5px;
    color: #343434;
    font-size: 12px;
    font-weight: 300;
`

const ButtonContainer = styled.div `
    display: flex;
    align-items: center;
    justify-content: center;
`

const Button = styled.button `
    align-content: center;
    margin-top: 20px;
    padding: 10px 15px;
    background-color: ${(props) => props.Color};
    border: none;
    color: white;
    border-radius: 7px;
    cursor: ${(props) => props.Cursor};
    box-sizing: border-box;
    
    &:hover {
        opacity: ${(props) => props.Hover};
    }
`

const LoginInputContainer = styled.div `
    display: flex;
    align-items: center;
    justify-content: center;
    width: 300px;
    height: 40px;
    padding: 1px;
    transition: 200ms linear;
    border-radius: 5px;

    &:focus-within {
        padding: 1px;
        box-shadow: 0 0 0 2px ${loginSelectors.boxShadowColor};
    }

`



const LoginInput = styled.input `
    width: 300px;
    height: 40px;
    outline: 0;
    box-sizing: border-box;
    border: 2px solid #838383;
    border-radius: 5px;
    font-size: 16px;
    padding-left: 10px;

    &:hover {
        border: 2px solid #042493;
    }
`

const RememberMeCheckBoxDiv = styled.div `
    display: flex;
    align-items: center;
    height: 30px;
`

const RememberMeCheckBox = styled.input `
    cursor: pointer;
    accent-color: #042493;
    margin-top: 0;
    
`

const RememberMeText = styled.div `
    display: flex;
    align-items: center;
    margin-left: 5px;
    font-size: 12px;
    font-weight: normal;
`

const WarningDiv = styled.div `
    width: 100%;
    background-color: rgba(255, 204, 0, 0.33);
    border: 1px solid darkorange;
    margin: 0 0 20px 0;
    box-sizing: border-box;
    padding: 10px;
    font-weight: normal;
    font-size: 14px;
    display: flex;
    flex-direction: row;
`

const WarningIcon = styled.div `
    margin-right: 8px;
`

const WarningText = styled.div `

`

const WarningClose = styled.div `
    cursor: pointer;
    margin-left: 8px;
    
    &:focus {
        transform: scale(.8);
    }
`
const Login = () => {

    //Use State Hooks

    const [passwordInput, setPasswordInput ] = useState({
        active: false,
        inputValue: ""
    })

    const [userInput, setUserInput] = useState({
        active: true,
        inputValue: ""
    })

    const [formButton, setFormButton] = useState({
        text: "Next",
        clickable: false,
    });

    const navigate = useNavigate()


    const [rememberMeActive, setRememberMeActive] = useState(false)
    const [securityWarningActive, setSecurityWarningActive] = useState(false)
    const {setLoggedIn, cookieUser, setCookieUser} = useContext(UserContext)

    const queryFetchLogin = () => {
        return Axios.post("/login", {
            headers: {
                'Content-Type': 'application/json;charset=UTF-8'
            },
            data: {
                userID: userInput.inputValue,
                password: passwordInput.inputValue
            },
        })
            .then(res => res.data)
    }
    const [enabled, setEnabled] = useState(false)


    const {data, isLoading, refetch} = useQuery(["user"], queryFetchLogin, {
        enabled: enabled,
        staleTime: Infinity,
    })


    //Handle Button Change

    const refPass = useRef();
    const handleLoginButton = async ()  => {
        setEnabled(true)
        if(formButton.text === "Next" && formButton.clickable === true){

            await setPasswordInput(prevState => ({...prevState, active: true}))
            await (setFormButton(prevState => ({...prevState, clickable: false, text: "Sign In"})))
            refPass.current.focus()
        } if(formButton.text === "Sign In" && formButton.clickable === true) {
            await refetch()
                .then(() => {
                    try {
                        if(data.userID === userInput.inputValue && passwordInput.inputValue === data.password){
                            document.cookie = `user=${userInput.inputValue}`
                            setCookieUser(data.userID)
                            setLoggedIn(true)
                            navigate("/")
                        } else {
                            alert("Nope")
                        }
                    }
                    catch (err) {
                        console.log(err)
                    }
                })
        }
         if(formButton.clickable === false) {
            alert("Please fill out User ID and Password")
        }
    }

    // Handle form inputs & user and password inputs updates
    const handleUsernameInputChange = (e) => {
        let inputData = e.target.value
        if(inputData !== ""){
            setFormButton(prevState => ({...prevState, clickable: true}))
            setUserInput(prevState => ({...prevState, inputValue: inputData}))
        } else {
            setFormButton(prevState => ({...prevState, clickable: false}))
        }
    }

    const handlePasswordInputChange = (e) => {
        let inputData = e.target.value
        if(inputData !== ""){
            setFormButton(prevState => ({...prevState, clickable: true}))
            setPasswordInput(prevState => ({...prevState, inputValue: inputData}))
        }  else {
            setFormButton(prevState => ({...prevState, clickable: false}))
        }
    }


    //Toggle security message
    const toggleCheckBox = () => {
        setRememberMeActive(!rememberMeActive)
        if(!rememberMeActive) {
          return  setSecurityWarningActive(true)
        }
    }



    useEffect(() => {
    }, [formButton, passwordInput, userInput, cookieUser])

    return (


        <LoginPageContainer>
            <LoginPageLogo src={Logo} alt={"adp run logo"} aria-label={"adp" +
                " run logo"} />
            <LoginContainer>
                <LockIconDiv>
                    <FaLock style={{color: "gray", fontSize: "10px"}}/>
                </LockIconDiv>
                <LoginHeader>Welcome!</LoginHeader>
                <InputContainer>


                    {/* Label User ID & User ID Input Field*/}
                        <Label>User ID</Label>
                        <LoginInputContainer>
                            <LoginInput
                                onChange={(e) => handleUsernameInputChange(e)}
                                onKeyDown={(e)  =>{
                                    if(e.key === "Enter") {
                                        console.log("pressed")
                                        handleLoginButton(e)
                                    }}}/>
                        </LoginInputContainer>



                    {/* Remember Me Checkbox*/}
                        <RememberMeCheckBoxDiv>
                            <div>
                                <RememberMeCheckBox type={"checkbox"} onClick={toggleCheckBox} />
                            </div>
                            <div>
                                <RememberMeText>
                                    Remember User ID <AiFillInfoCircle color={"#042493"} style={{marginLeft: "3px"}} />
                                </RememberMeText>
                            </div>

                        </RememberMeCheckBoxDiv>



                    {/* Remember Me Checkbox Warning Text*/}
                        {securityWarningActive && rememberMeActive ?
                            <WarningDiv>
                            <WarningIcon><AiFillWarning color={"darkorange"} style={{fontSize: "20px"}}/></WarningIcon>
                            <WarningText>
                                Select this option on your private or personal
                                device. Do not use this option on devices shared
                                with other users in your company.
                            </WarningText>
                            <WarningClose onClick={() =>{
                                setSecurityWarningActive(false)

                            }}>
                                <AiOutlineClose style={{fontSize: "20px"}}/>
                            </WarningClose >

                        </WarningDiv> : <></> }



                    {/* Label Password & Password Input Field*/}
                        {passwordInput.active ?
                            <>
                            <Label>Password</Label>
                            <LoginInputContainer>
                                <LoginInput
                                    ref={refPass}
                                    type={"password"}
                                    onChange={(e) => {
                                        handlePasswordInputChange(e)
                                    }}
                                    onKeyDown={(e)  =>{
                                        if(e.key === "Enter") {
                                            console.log("pressed")
                                            handleLoginButton(e)
                                            refPass.current.focus()
                                        }}}

                                />
                            </LoginInputContainer>
                            </> : <></>}
                <ButtonContainer>
                    <Button
                        Color={formButton.clickable ? "#042493" : "#838383"}
                        Cursor={formButton.clickable ? "pointer" : "not-allowed"}
                        Hover={formButton.clickable ? ".8" : "1"}
                        onClick={handleLoginButton}
                    >
                        {formButton.text}
                    </Button>
                </ButtonContainer>
                </InputContainer>
            </LoginContainer>
        </LoginPageContainer>
    );
}

export default Login