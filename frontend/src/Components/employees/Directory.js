import React from 'react';
import styled from "styled-components/macro";

const DirectoryContainer = styled.div `
    background: inherit;
`
const Directory = () => {

    return (
        <DirectoryContainer>
            <h1>Directory</h1>
        </DirectoryContainer>
    );
};

export default Directory;