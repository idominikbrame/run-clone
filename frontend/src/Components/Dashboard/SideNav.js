import React, {useContext, useEffect, useState} from 'react';
import styled from "styled-components/macro";
import {BsFillHouseDoorFill, BsFillPeopleFill} from 'react-icons/bs'
import {AiFillDollarCircle} from 'react-icons/ai'
import {GiHamburgerMenu} from 'react-icons/gi'
import {Link} from 'react-router-dom'
import {UserContext} from "../../context/UserContext";
import {StyleContext} from "../../context/StylingContext";

const sideNavList = [
    {
        text: "Home",
        icon: <BsFillHouseDoorFill />,
        route: ""
    },
    {
        text: "Payroll",
        icon: <AiFillDollarCircle />,
        route: "payroll"
    },
    // {
    //     text: "Directory",
    //     icon:<BsFillPeopleFill />,
    //     route: "directory"
    // },
]


const SideNavContainer = styled.div `
    position: fixed;
    width: ${(props) => props.NavBarWidth};
    height: 100vh;
    background-color: #22228f;
    display: flex;
    flex-direction: column;
    justify-content: left;
    align-items: flex-start;
    color: white;
    transition: width 300ms ease-in-out;
    overflow: hidden;

`

const SideNavUl = styled.ul `
        margin: 0;
        padding-left: 1.2rem;
        height: 300px;
        display: flex;
        flex-direction: column;
        justify-content: space-evenly;
        list-style: none;
        text-decoration: none;
    `

const ListItem = styled(Link) `
    display: flex;
    flex-direction: row;
    align-items: center;
    padding: 10px 50px 10px 10px;
    border-radius: 5px;
    user-select: none;
    cursor: pointer;
    user-select: none;
    text-decoration: none;
    color: white;

    &:hover {
        background-color: #3333d7;
    }
`

const NavTextItem = styled.div `
    padding-left: 1.1rem;
`

const SideNav = () => {
    const {setToggleMenu, toggleMenu} = useContext(StyleContext)


    useEffect(() => {

    }, [toggleMenu])

    return (
            <SideNavContainer
                NavBarWidth={toggleMenu ? "200px" : "100px"}>
                <GiHamburgerMenu
                    style={{cursor:"pointer", fontSize: "30px", padding: "15px", margin: 0}}
                    onClick={() => setToggleMenu(!toggleMenu)}>Toggle</GiHamburgerMenu>
                <SideNavUl>
                    {sideNavList.map((item, index) =>{
                       return(
                           <ListItem key={index} to={item.route}>
                               <div  style={{fontSize: "22px"}}>{item.icon}</div><>{toggleMenu ?  <NavTextItem>{item.text}</NavTextItem> : <></> }</>

                           </ListItem>
                       )
                    })}
                </SideNavUl>

            </SideNavContainer>

    );
};

export default SideNav;