import React, {useRef, useState} from 'react';
import styled from "styled-components/macro";
import {useNavigate} from "react-router";
import {Link} from "react-router-dom";

const HomeContainer = styled.div`
    position: relative;
    background: none;
    display: flex;
    flex-direction: column;
    height: 100%;
    width: 100%;
    max-width: 1400px;
    align-content: center;
    justify-content: center;
    align-items: center;
    padding: 20px 0 20px 0;

`
const CardsContainer = styled.div`
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    height: 100%;
    gap: 20px;
    z-index: 0;
    width: 100%;
    

`

const DashboardPayrollContainer = styled.div`
    display: flex;
    min-width: 750px;
    width: 70%;
    height: 20rem;
    background-color: white;
    border-radius: .5rem;
    box-shadow: 0 4px 10px .5px rgba(147, 147, 147, 0.49);
    box-sizing: border-box;
    flex-direction: column;
    overflow: hidden;

`


const UpcomingContainer = styled.div`
    min-width: 100%;
    height: 6rem;
    display: flex;
    justify-content: center;
    align-items: center;
    color: black;
    
   

`

const UpcomingTextContainer = styled.div`
    width: 90%;
    border-bottom: 1px solid lightgrey;
`

const PayrollOptionsContainer = styled.div`
    display: flex;
    width: 100%;
    height: 100%;

`

const PayrollSelectorContainer = styled.div`
    flex: 30%;
    display: flex;
    flex-direction: column;
    border-right: 1px solid lightgrey;
    height: 14rem;
    margin-top: .5rem;
    padding-right: 1rem;

`

const FrequencyContainer = styled.div`
    display: flex;
    height: 40px;
    align-items: center;
    justify-content: flex-start;
    color: black;
    width: 100%;
    margin: 0 1rem;

`

const DueInContainer = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    background-color: #eaeae2;
    height: 1.5rem;
    border-radius: 2rem;
    padding: 0 .5rem;
    margin: 1rem;
`

const ScheduleContainer = styled.div`
    display: flex;
    flex-direction: row;
    width: 100%;
    height: auto;
    margin:1rem 0 2rem 0;
    color: black;
    letter-spacing: 1px;
`

const CheckDateContainer = styled.div`
    width: 50%;
    height: 4.5rem;
    border-radius: 1.5rem;
    background-color: #eaeae2;
    margin-left: 1rem;
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    justify-content: space-evenly;
    padding: 0 0 0 4rem;
    letter-spacing: 1px;


    &h2, h3, h6 {
        margin: 0;
    }

`


const PayPeriodContainer = styled.div`
    width: 50%;
    height: 4.5rem;
    border-radius: 1.5rem;
    background-color: #eaeae2;
    margin-left: 1rem;
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    justify-content: space-evenly;
    padding: 0 4rem;


    &h2, h3, h6 {
        margin: 0;
    }
`
const RunPayrollButtonContainer = styled.div`
    width: 100%;
    display: flex;
    align-items: flex-start;
    justify-content: center;
`

const RunPayrollButton = styled(Link)`
    width: 95%;
    height: 2.5rem;
    background-color: #0505b0;
    color: white;
    display: flex;
    justify-content: center;
    align-items: center;
    border-radius: .5rem;
    cursor: pointer;
    transition: 200ms ease-out;
    margin: 0 0 0 1rem;
    text-decoration: none;
    opacity: .9;

    & p {
        font-size: 15px;
    }

    &:hover {
        opacity: 1;
    }

`



const SearchInput = styled.input `
    width: ${(props) => props.searchWidth};
    padding: 10px 0 10px 15px;
    border-radius: 1.5rem;
    outline: none;
    border: none;
    font-size: 1rem;
    color: black;
    transition: 500ms ease-out;
    scroll-behavior: smooth;
    margin: 20px 0 40px 0;
    position: sticky;
    max-width: 800px;

    
   
    

    ::placeholder {
        color: rgba(0, 0, 0, 0.46);
    }

`

const WelcomeText = styled.div `
    
`

const SearchContainer = styled.div `
    width: inherit;
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    align-items: center;
    z-index: 1001;
    position: sticky;
    top: -80px;
    
`


const Home = () => {

    const numberRef = useRef(0)
    console.log(numberRef.current)

    const [scrollPosition, setScrollPosition] = useState(0)

    const handleScroll = () => {
        const position = window.scrollY;
        setScrollPosition(position)
    }

    window.addEventListener(('scroll'), handleScroll, {passive: true})

    return (
        <>
            <SearchContainer>
                <WelcomeText>
                    <h1>Hello, Dominik.</h1>
                </WelcomeText>
                <SearchInput placeholder={"How can we help you today?"}
                             searchWidth={scrollPosition >= 180 ? "30%" : "50%"}
                />
            </SearchContainer>
        <HomeContainer>
            <CardsContainer>
                <DashboardPayrollContainer>

                    <UpcomingContainer>
                        <UpcomingTextContainer>
                            <h3>Upcoming Payroll</h3>
                        </UpcomingTextContainer>
                    </UpcomingContainer>

                    <PayrollOptionsContainer>
                        <PayrollSelectorContainer>
                            <FrequencyContainer>
                                <h2>Weekly</h2>
                                <DueInContainer>
                                    <p>Due In 5 Days</p>
                                </DueInContainer>
                            </FrequencyContainer>
                            <ScheduleContainer>
                                <CheckDateContainer>
                                    <h6>Check Date</h6>
                                    <h3>11/20/2022</h3>
                                </CheckDateContainer>
                                <PayPeriodContainer>
                                    <h6>Pay Period</h6>
                                    <h3>11/10-11/17</h3>
                                </PayPeriodContainer>
                            </ScheduleContainer>
                            <RunPayrollButtonContainer>
                                <RunPayrollButton to={"/payroll"}>
                                    <p>Run Payroll</p>
                                </RunPayrollButton>
                            </RunPayrollButtonContainer>
                        </PayrollSelectorContainer>
                    </PayrollOptionsContainer>
                </DashboardPayrollContainer>
            </CardsContainer>
        </HomeContainer>
        </>
    );
};

export default Home;