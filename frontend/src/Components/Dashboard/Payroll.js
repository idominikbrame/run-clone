import React, {useEffect, useRef, useState} from 'react';
import styled from "styled-components/macro";
import {BiNotepad} from 'react-icons/bi'
import {RiPencilFill} from 'react-icons/ri'
import PayrollPreview from "./PayrollPreview";
import {useQuery} from "@tanstack/react-query";
import Axios from "axios";


const PayrollContainer = styled.div`
    width: 100%;
    min-height: 100vh;
    background-color: #eeeeee;
    z-index: -1;
    display: flex;
    flex-direction: column;
    align-items: center;


`

const DescriptionColumnsContainer = styled.div`
    margin-top: .5em;
    width: 95%;
    border-radius: 7px;
    background-color: #e7e7e7;
    flex-direction: row;
    gap: 0;
    z-index: -100;

`

const ColumnDescriptionsTable = styled.table`
    padding: 0;
    color: black;
    width: 100%;
    scroll-behavior: smooth;
    background-color: #e7e7e7;


    td, th {
        background-color: white;
        padding: 10px;
        min-width: 200px;
        line-height: .8;
    }
    
    

    input {
        background-color: transparent;
        border: none;
        outline: none;
        text-align: center;
        font-size: 1.5rem;

    }
`
const EmployeeTableRow = styled.tr`
    background-color: black;
    color: #2f6bb4;
    text-align: left;
    font-size: 1.5rem;
    margin-left: 3rem;


    span {
        color: #1e1e1e;
        margin: 0;
        padding: 0;
        font-weight: normal;
    }


    :focus, :focus-within, :hover {
        td {
            background-color: #f2f6ff;
            text-align: left;

        }

    }

    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    td {
        background-color: white;
        outline: none;
        min-width: 100px;
     }

`

const PayRateInput = styled.input`
    width: 100%;
    background-color: transparent;


    :focus {
        width: 70%;
        text-align: right;

    }
`

const ProgressContainer = styled.div `
    width: 100%;
    height: min-content;
    padding: 40px 0 40px 0;
    background-color: white;
    top: 80px;
    z-index: 1;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    contain: content;
    
`

const ProgressChildrenContainer = styled.div `
    padding: 0;
    width: 100%;
    height: auto;
    display: flex;
    flex-direction: row;
    justify-content: space-around;
    align-items: center;
    color: black;
    margin-left: 25%;
`

const ProgressPoints1 = styled.div `
    height: 30px;
    min-width: 30px;
    background-color: #2f6bb4;
    border-radius: 15px;
    display: block;
    box-sizing: border-box;
`
const ProgressPoints2 = styled.div `
    height: 30px;
    width: 30px;
    background-color:  ${(props) => props.BackgroundColor};
    border-radius: 50%;
    display: block;
    box-sizing: border-box;
    border: 4px solid #2f6bb4;
`

const ProgressPoints3 = styled.div `
    height: 30px;
    width: 30px;
    background-color: white;
    border-radius: 50%;
    box-sizing: border-box;
    display: block;
    
    border: 4px solid #a8a8a8;
`

const ProgressBars1 = styled.div `
    width: 100%;
    height: 4px;
    background-color: #2f6bb4;
`
const ProgressBars2 = styled.div `
    width: 100%; 
    height: 4px;
    background-color:  ${(props) => props.BackgroundColor};
    
`

const ProgressUIContainer = styled.div `
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: flex-start;
    width: 100%;
    margin-left: 30px;
`

const ProgressChildContainer = styled.div `
    display: flex;
    flex-direction: column;
    width: 100%;
`

const EnterPayrollText = styled.div `
    
`

const PayrollSpecificsContainer = styled.div `
    display: flex;
    flex-direction: row;
    align-items: center;
    height: 10%;
    min-height: 100px;
    width: 95%;
    margin-top: 1em;
    color: black;

    &h2, h3, h4, h6, p {
        margin: 0;
    }
`

const PayFrequencyText = styled.div `
    font-size: 2em;
    font-weight: bolder;
    height: 100%;
    justify-content: flex-start;
    color: black;
`

const PayPeriodContainer = styled.div `
    max-width: 200px;
    min-width: 200px;
    width: 25%;
    border-radius: .3em;
    background-color: white;
    height: 4em;
    margin-left: 5%;
    display: flex;
    flex-direction: column;
    align-self: flex-start;
    justify-content: space-evenly;
    padding-left: 20px;
    position: relative;
    
    h3, h4, h5, p  { 
        padding: 0;
        margin: 0;
    }
`

const PayrollAddressContainer = styled.div `
    max-width: 450px;
    min-width: 200px;
    width: 25%;
    border-radius: .3em;
    background-color: white;
    height: 4em;
    margin-left: 5%;
    display: flex;
    flex-direction: column;
    align-self: flex-start;
    justify-content: space-evenly;
    padding-left: 20px;
    position: relative;
    
    h4, h5 {
        margin: 0;
        
    }
`

const HelpContainer = styled.div  `
    flex: 1;
    display: flex;
    height: 100%;
    align-items: flex-start;
    justify-content: flex-end;
`

const NeedHelp = styled.div `
    background-color: #cbdbf6;
    width: 150px;
    height: 25px;
    border-radius: 3em;
    display: flex;
    flex-direction: row;
    align-items: center;
    cursor: pointer;
    margin-left: 10px;

    p {
        color: black;
    }


`

const BlueDot = styled.div `
    width: 18px;
    height: 18px;
    margin: 10px;
    border-radius: 10em;
    background-color: midnightblue;
`

const PayrollButtonsContainer = styled.div `
    display: flex;
    justify-content: flex-end;
    flex-direction: row;
    width: 95%;
    height: 70px;
    align-items: center;
`

const PreviewPayrollButton = styled.input `
    background-color: blue;
    color: white;
    font-size: 1em;
    border: none;
    outline: none;
    cursor: pointer;
    opacity: .8;
    border-radius: 1em;
    padding: .5em 1em;
    margin-right: 1em;
    text-decoration: none;
    display: flex;
    
    Link {
        text-decoration: none;
        
    }
    
    :hover {
        opacity: 1;
    }
`
const SavePayrollButton = styled.div `
    background-color: white;
    color: black;
    font-size: 1em;
    border: 2px solid darkblue;
    outline: none;
    cursor: pointer;
    opacity: .8;
    border-radius: 1em;
    padding: .5em 1em;
    margin-right: 1em;
    text-decoration: none;
    display: flex;

    :hover {
        opacity: 1;
    }
`



let columnDescriptions = [
    {
        name: "Employee"
    },
    {
        name: "Pay Rate"
    },
    {
        name: "Hours"
    },
    {
        name: "Salary"
    },
]

const Payroll = () => {

    const updateSalary = (event, index) => {
        let inpVal = Number(event.target.value);
        if (isNaN(inpVal)) {
            event.target.value = employees[index].salary;
        } else if (inpVal < 999999 && event.target.value.length < 9) {
            const newEmployeeState = employees.map(obj => {
                if (obj.id === 1 + index && inpVal < 999999) {
                    return {
                        ...obj,
                        salary: inpVal
                    }
                }
                return obj
            })
            setEmployees(newEmployeeState)
        } else {
            event.target.value = employees[index].salary
        }
    }
    const updatePayRate = (event, index) => {
        let inpVal = Number(event.target.value);
        if (isNaN(inpVal)) {
            event.target.value = employees[index].payRate;
        } else if (inpVal < 150 && event.target.value.length < 6) {
            const newEmployeeState = employees.map(obj => {
                if (obj.id === 1 + index && inpVal < 145) {
                    return {
                        ...obj,
                        payRate: inpVal
                    }
                }
                return obj
            })
            setEmployees(newEmployeeState)
        } else {
            event.target.value = employees[index].payRate
        }
    }


    const earnings = columnDescriptions.map((name) => name)
    earnings.splice(0, 2)


    const [employees, setEmployees] = useState([])
    const [fetched, setFetched] = useState(false)
    const queryFetchEmployees = () => {
        return Axios.get("/employees")
            .then(res => res.data)
    }

    const {status,data, isLoading, refetch} = useQuery(["employees"], queryFetchEmployees, {
        enabled: true,
        staleTime: Infinity,
    })



    useEffect(() => {
        if(status === 'success' && !fetched){
            setEmployees(data)
            setFetched(true)
        }

        console.log(employees)
    }, [data, status, employees])

    const numberToCurrency = (number) => {
        let strToNum = Number(number)
        if(strToNum > 999 ){
            return `$${strToNum.toLocaleString(undefined, {maximumFractionDigits:2})}`
        }
        if(isNaN(Number(number)) || number === '' || number === 0){
            return ""
        }
        return `$${Number(number).toFixed(2)}`
    }

    const [previewToggle, setPreviewToggle] = useState(true)
    const updateEmployee =(event, index) => {
        let inpVal = Number(event.target.value);
        console.log(typeof event.target.value)
        if (isNaN(inpVal)) {
            event.target.value = employees[index].standardHours;
        } else if (inpVal < 150 && event.target.value.length < 6) {
            const newEmployeeState = employees.map(obj => {
                console.log(obj.id)
                console.log(index + 1)
                if (obj.id === 1 + index && inpVal < 145) {
                    console.log('some')
                    return {
                        ...obj,
                        standardHours: inpVal.toFixed(2)
                    }
                }
                return obj
            })
            setEmployees(newEmployeeState)
            console.log(newEmployeeState)
        } else {
            event.target.value = employees[index].standardHours
        }
    }

    const showPayrollPreview = async () => {
      setPreviewToggle(!previewToggle)
        console.log(previewToggle)

    }

    if(isLoading){
        return (
            <div>Loading...</div>
        )
    }

    return (
                <PayrollContainer>
                    <ProgressContainer>
                        <ProgressChildrenContainer>
                            <ProgressChildContainer>
                                <EnterPayrollText>Enter
                                    Payroll</EnterPayrollText>
                                <ProgressUIContainer>
                                    <ProgressPoints1></ProgressPoints1>
                                    <ProgressBars1></ProgressBars1>
                                </ProgressUIContainer>
                            </ProgressChildContainer>


                            <ProgressChildContainer>
                                <EnterPayrollText>
                                    Preview Payroll</EnterPayrollText>
                                <ProgressUIContainer>
                                    <ProgressPoints2
                                        BackgroundColor={previewToggle ? "white" : "#2f6bb4"}
                                    ></ProgressPoints2
                                    >
                                    <ProgressBars2
                                        BackgroundColor={previewToggle ? "#ccc6c6" : "#2f6bb4"}
                                    ></ProgressBars2>
                                </ProgressUIContainer>
                            </ProgressChildContainer>


                            <ProgressChildContainer>
                                <EnterPayrollText>Payroll Processed</EnterPayrollText>
                                <ProgressUIContainer>
                                    <ProgressPoints3

                                    ></ProgressPoints3>
                                </ProgressUIContainer>
                            </ProgressChildContainer>

                        </ProgressChildrenContainer>
                    </ProgressContainer>
                    {previewToggle ?
                        <>
                    <PayrollSpecificsContainer>
                        <PayFrequencyText>
                            <div>
                                Biweekly
                            </div>
                        </PayFrequencyText>
                        <PayPeriodContainer>
                            <p>Pay Period</p>
                            <h5>11/10-11/17</h5>
                            <RiPencilFill style={{
                                cursor: "pointer",
                                fontSize: "20px",
                                position: "absolute",
                                top: "3px",
                                right: "5px"
                            }} onClick={() => console.log("clicked")}/>
                        </PayPeriodContainer>
                        <PayrollAddressContainer>
                            <p>Payroll Address</p>
                            <h5>2619 Willow Circle, Henderson, IL, 60081</h5>
                            <RiPencilFill style={{
                                cursor: "pointer",
                                fontSize: "20px",
                                position: "absolute",
                                top: "3px",
                                right: "5px"
                            }} onClick={() => console.log("clicked")}/>
                        </PayrollAddressContainer>
                        <HelpContainer>
                            <BiNotepad
                                style={{color: "blue", fontSize: '25px'}}/>
                            <NeedHelp>
                                <BlueDot>

                                </BlueDot>
                                <p>Need Help?</p>
                            </NeedHelp>
                        </HelpContainer>
                    </PayrollSpecificsContainer>
                    <DescriptionColumnsContainer>
                        <ColumnDescriptionsTable>
                            <tbody>
                            <tr>
                                {columnDescriptions.map((column, index) => {
                                    return (
                                        <th key={index}>
                                            {column.name}
                                        </th>
                                    )
                                })}
                            </tr>
                            </tbody>
                            {employees.map((ee, index) => {
                                return (
                                    <tbody key={index}>
                                    <EmployeeTableRow tabIndex={0}>
                                        <td>{ee.name}</td>
                                        <td>
                                            {ee.payType === "Hourly" ?
                                                <PayRateInput
                                                    type={"text"}

                                                    onFocus={(event) => {
                                                        event.target.value = ee.payRate
                                                        event.target.select()
                                                    }}

                                                    onChange={(event) => {
                                                        updatePayRate(event,
                                                            index)
                                                    }}

                                                    onBlur={(event) => {
                                                        event.target.value = numberToCurrency(
                                                            employees[index].payRate) + '/hr'
                                                    }}
                                                    defaultValue={numberToCurrency(
                                                        ee.payRate) + '/hr'}
                                                /> : <PayRateInput
                                                    onChange={(event) => updatePayRate(
                                                        event, index)}
                                                    onFocus={(event) => {
                                                        event.target.value = ee.payRate
                                                        event.target.select()
                                                    }}
                                                    onBlur={(event) => {
                                                        if (event.target.value === '' || event.target.value === '0') {
                                                            return event.target.value = ''
                                                        }
                                                        event.target.value = numberToCurrency(
                                                            employees[index].payRate) + '/hr'
                                                    }}
                                                    defaultValue={numberToCurrency(
                                                        ee.payRate)}/>}
                                        </td>
                                        <td>
                                            <PayRateInput
                                                onChange={(event) => updateEmployee(event, index)}
                                                onFocus={(event) => {
                                                    event.target.value = ee.standardHours > 0 ? ee.standardHours : null
                                                    event.target.select()
                                                }}
                                                onBlur={(event) => {
                                                    if (event.target.value === '' || event.target.value === '0') {
                                                        return event.target.value = ''
                                                    }
                                                    event.target.value = Number(
                                                        event.target.value).toFixed(
                                                        2)
                                                }}
                                                defaultValue={ee.standardHours > 0 ? Number(
                                                    ee.standardHours).toFixed(
                                                    2) : null}/>
                                        </td>
                                        <td>
                                            {ee.payType === "Salary" ?
                                                <input
                                                    onChange={(event) => updateSalary(
                                                        event, index)}
                                                    defaultValue={numberToCurrency(
                                                        ee.salary)}
                                                    type={"text"}
                                                    onFocus={(event) => {
                                                        event.target.value = ee.salary
                                                        event.target.select()
                                                    }}
                                                    onBlur={(event) => {
                                                        if (event.target.value === '' || event.target.value === '0') {
                                                            return event.target.value = ''
                                                        }
                                                        event.target.value = numberToCurrency(
                                                            employees[index].salary)
                                                    }}/>
                                                : <input
                                                    onChange={(event) => updateSalary(
                                                        event, index)}
                                                    defaultValue={numberToCurrency(
                                                        ee.salary)}
                                                    type={"text"}
                                                    onFocus={(event) => {
                                                        event.target.value = ee.salary
                                                        event.target.select()
                                                    }}
                                                    onBlur={(event) => {
                                                        if (event.target.value === '' || event.target.value === '0') {
                                                            return event.target.value = ''
                                                        }
                                                        event.target.value = numberToCurrency(
                                                            employees[index].salary)
                                                    }}/>}
                                        </td>
                                    </EmployeeTableRow>
                                    </tbody>
                                )
                            })}
                        </ColumnDescriptionsTable>


                    </DescriptionColumnsContainer>
                    <PayrollButtonsContainer>
                        <PreviewPayrollButton
                            type={"button"}
                            value={"Payroll Preview"}
                            onClick={() => showPayrollPreview()}
                        />

                    </PayrollButtonsContainer>
                        </>
                        : <PayrollPreview togglePreview={showPayrollPreview} employeeData={employees}/>}
                </PayrollContainer>

    );
};

export default Payroll;