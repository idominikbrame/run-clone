import React, {useEffect, useState} from 'react';
import styled from "styled-components/macro";
import {useNavigate} from "react-router";

const PreviewContainer = styled.div `
    position: relative;
    color: black;
    width: 100%;
    max-width: 1000px;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
`
const PreviewTable = styled.table `
    width: 100%;

    td, th {
        background-color: white;
        padding: 10px;
        min-width: 200px;
        line-height: .8;
    }
`

const EmployeeTableRow = styled.tr`
    background-color: black;
    color: #3d3d3d;
    text-align: left;
    font-size: 1.5rem;
    margin-left: 3rem;


    td {
        background-color: white;
        outline: none;
        min-width: 100px;
        width: 100%;
        text-align: center;
    }

`

const PayrollButtonsContainer = styled.div `
    display: flex;
    justify-content: flex-end;
    flex-direction: row;
    width: 95%;
    height: 70px;
    align-items: center;
`

const PreviewPayrollButton = styled.input `
    background-color: blue;
    color: white;
    font-size: 1em;
    border: none;
    outline: none;
    cursor: pointer;
    opacity: .8;
    border-radius: 1em;
    padding: .5em 1em;
    margin-right: 1em;
    text-decoration: none;
    display: flex;
    
    Link {
        text-decoration: none;
        
    }
    
    :hover {
        opacity: 1;
    }
`

const LoadingDiv = styled.div `
    
    position: fixed;
    top: 0;
    left: 0;
    width: 100vw;
    height: 100vh;
    display: flex;
    align-items: center;
    justify-content: center;
    transition: opacity .75s, visibility .57s;
    
    
    :after {
        visibility: ${(props) => props.Hidden};
        content: "";
        width: 75px;
        height: 75px;
        border: 15px solid #dddddd;
        border-top-color: darkblue;
        border-radius: 50%;
        animation: loading 1s ease infinite;
    }
    
    @keyframes loading {
        from {transform: rotate(0turn)}
        to { transform: rotate(1turn)}
    }
    
`

const HiddenDiv = styled.div `
    display: none;
`
const SavePayrollButton = styled.div `
    background-color: white;
    color: black;
    font-size: 1em;
    border: 2px solid darkblue;
    outline: none;
    cursor: pointer;
    opacity: .8;
    border-radius: 1em;
    padding: .5em 1em;
    margin-right: 1em;
    text-decoration: none;
    display: flex;

    :hover {
        opacity: 1;
    }
`



let previewColumnDescriptions = [
    {
        name: "Employee"
    },
    {
        name: "Gross Wages"
    },
    {
        name: "Taxes"
    },
    {
        name: "Net Pay"
    },
]

const PayrollPreview = (props) => {
    const navigate = useNavigate()
    const [loadingComplete, setLoadingComplete] = useState(false)

    const processPayroll = async () => {
        setLoadingComplete(true)
        await setTimeout(() => {
            setLoadingComplete(false)
            alert("Payroll Processed")
            navigate("/")
        }, 2000)
    }

    useEffect(() => {
        console.log(loadingComplete)
    },[loadingComplete])

    return (
        <PreviewContainer>
            {loadingComplete ? <LoadingDiv></LoadingDiv> : <HiddenDiv></HiddenDiv>}
            <h2>Payroll Preview</h2>
            <PreviewTable>
            <tbody>
            <tr>
                {previewColumnDescriptions.map((column, index) => {
                    return (
                        <th key={index}>
                            {column.name}
                        </th>
                    )
                })}
            </tr>
            </tbody>
                {props.employeeData.map((ee, index) => {
                    console.log(typeof ee.salary)
                    console.log(typeof ee.standardHours)

                    const getEmployeeGross = () => {
                        let gross;
                        if(ee.salary){
                        gross = ee.salary
                        } else {
                            gross = ee.payRate * ee.standardHours
                        }
                        return gross.toFixed(2)
                    }

                    const getTaxes = () => {
                        let gross = getEmployeeGross()
                        let taxes = gross * .13
                        return taxes.toFixed(2)
                    }

                    const getNetPay = () => {
                        let gross = getEmployeeGross()
                        let taxes = gross * .13
                        return (gross - taxes).toFixed(2)
                    }
                    return (
                        <>
                        {ee.salary !== 0 || ee.standardHours !== 0 ?
                        <tbody key={index}>
                        <EmployeeTableRow tabIndex={0}>
                            <td>{ee.name}</td>
                            <td>
                                {getEmployeeGross()}
                            </td>
                            <td>
                                {getTaxes()}
                            </td>
                            <td>
                                {getNetPay()}
                            </td>
                        </EmployeeTableRow>
                        </tbody> : <></>}
                        </>)
                })}
            </PreviewTable>
            <PayrollButtonsContainer>
                <SavePayrollButton
                    type={"button"}
                    onClick={props.togglePreview}

                > Back
                </SavePayrollButton>
                <PreviewPayrollButton
                    type={"button"}
                    value={"Submit Payroll"}
                    onClick={() => {
                        processPayroll()
                    }}
                />

            </PayrollButtonsContainer>
        </PreviewContainer>
    );
};

export default PayrollPreview;