import React, {useEffect, useRef, useState} from 'react';
import styled from "styled-components/macro";
import {AiOutlineSearch} from 'react-icons/ai'


const TopNavContainer = styled.div`
    position: sticky;
    width: 100%;
    top: 0;
    left: 0;
    display: flex;
    justify-content: space-between;
    align-items: center;
    height: 5rem;
    background-color: midnightblue;
    z-index: 1000;
`

const QuickSettingsContainer  = styled.div `
    display: flex;
    width: 200px;
    margin-right: 1rem;
    flex-direction: row;
    justify-content: space-between;
    
`

const CompanyInfo = styled.div `
    margin : 1rem;
    padding: 0;
    
`







const TopNav = () => {



    return (
        <>
        <TopNavContainer>
            <CompanyInfo>
                <h4>Company Name</h4>
            </CompanyInfo>
            <QuickSettingsContainer>
                <div>Login</div>
                <div>Support</div>
                <div>Profile</div>
            </QuickSettingsContainer>
        </TopNavContainer>

        </>

    );
};

export default TopNav;