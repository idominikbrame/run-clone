import React, {useEffect, useState} from 'react';
import SideNav from "./SideNav";
import styled from "styled-components/macro";
import {Routes, Route} from "react-router";
import Home from "./Home";
import Payroll from "./Payroll";
import Directory from "../employees/Directory";
import TopNav from "./TopNav";
import {StyleContext} from "../../context/StylingContext";
import {NavContext} from "../../context/NavContext";


const DashBoardContainer = styled.div`
    height: 100vh;
    contain: content;
    width: auto;
    display: flex;
    flex-direction: column;
    user-select: none;
    text-decoration: none;
    color: white;
    background: linear-gradient(180deg, midnightblue 20rem, whitesmoke 0);
    margin-left: ${(props) => props.DashboardPaddingLeft};
    transition: 300ms ease-in-out;
`
const ComponentDiv = styled.div`
    display: flex;
    position: inherit;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    width: 100%;
    height: auto;
`



const Dashboard = () => {
    const [toggleMenu, setToggleMenu] = useState(false)
    const [activeComponent, setActiveComponent] = useState({
        activeComponent: "",
        showSearch: false
    })
    useEffect(() => {

    }, [toggleMenu])

    return (
        <StyleContext.Provider value={{toggleMenu, setToggleMenu}}>
            <NavContext.Provider value={{activeComponent, setActiveComponent}}>
            <SideNav/>

            <DashBoardContainer
                DashboardPaddingLeft={toggleMenu ? "200px" : "70px"}>
                <TopNav />
            <ComponentDiv>

                <Routes>
                    <Route exact path={"/"} element={<Home/>}/>
                </Routes>
            </ComponentDiv>
                <Routes>
                    <Route exact path={"/Payroll"} element={<Payroll/>}/>
                    <Route exact path={"/Directory"} element={<Directory />}/>
                </Routes>
        </DashBoardContainer>
            </NavContext.Provider>
            </StyleContext.Provider>

    );
};

export default Dashboard;