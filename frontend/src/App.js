import './App.css';
import {Routes, Route} from "react-router";
import Login from "./Components/Login/Login";
import ProtectedRoutes from "./Components/ProtectedRoutes";
import Dashboard from "./Components/Dashboard/Dashboard";
import Layout from "./Components/Layout";
import Missing from "./Components/Missing";
import {UserContext} from "./context/UserContext";
import React, {useState} from "react";
import People from "./Components/Dashboard/People";
import Payroll from "./Components/Dashboard/Payroll";
import SideNav from "./Components/Dashboard/SideNav";
import styled from "styled-components/macro";
import {
    QueryClientProvider
} from "@tanstack/react-query";
import {QueryClient} from "@tanstack/react-query";

const client = new QueryClient()

const Container = styled.div`
    position: relative;
    
`

function App() {

    const [loggedIn, setLoggedIn] = useState(true)
    const [cookieUser, setCookieUser] = useState("")

    return (
        <QueryClientProvider client={client}>
            <UserContext.Provider
                value={{loggedIn, setLoggedIn, cookieUser, setCookieUser}}>
                <Container>
                    <Routes>

                        <Route path={"*"} element={<Layout/>}>
                            {/*Public Routes*/}
                            <Route path={"login"} element={<Login/>}/>

                            {/*Protected Routes*/}
                            <Route element={<ProtectedRoutes/>}>
                                <Route exact path={"*"} element={<Dashboard/>}/>

                            </Route>


                        </Route>

                    </Routes>
                </Container>
            </UserContext.Provider>
        </QueryClientProvider>

    );
}

export default App;
