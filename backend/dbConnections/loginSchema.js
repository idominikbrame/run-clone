const mongoose = require('mongoose');
const {Schema, model} = require('mongoose');

const LoginSchema = new Schema({
    userID: String,
    password: String
})

const Login = model('users', LoginSchema);
module.exports = Login;