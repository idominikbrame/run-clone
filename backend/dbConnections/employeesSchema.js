const mongoose = require('mongoose');
const {Schema, model} = require('mongoose');

const EmployeesSchema = new Schema({
    id: Number,
    payType: String,
    name: String,
    salary: Number,
    payRate: Number,
    standardHours: Number,
    currentPayPeriodStandHours: Number,
    address: Object
})

const Employees = model('employees', EmployeesSchema);
module.exports = Employees;