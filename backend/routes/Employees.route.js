const express = require('express');
const router = express.Router();
const EmployeeModel = require('../dbConnections/employeesSchema');


router.get("/employees", (req, res, next) => {
    EmployeeModel.find({}, (err, result) => {
        if(result === null) {
            res.json({message: null})
            return
        } else {
            res.json(result)
            return
        }
    })
});

module.exports = router