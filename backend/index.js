const express = require("express")
const app = express()
const cors = require("cors");


//Routes
const router = require('./routes/Login.route')
const LoginRoute = require('./routes/Login.route')
const EmployeesRoute = require('./routes/Employees.route')
const PayrollRoute = require('./routes/Payroll.route')

app.use(cors());
app.use(express.json())

//Import Routes
app.use(LoginRoute)
app.use(EmployeesRoute)
app.use(PayrollRoute)


//Start Server / Connect to Database
const port = process.env.PORT || 5001
const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/run-clone')
console.log(mongoose.connection.readyState);
app.listen(port, () => console.log("Backend server live on " + port));
